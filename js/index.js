let items = document.getElementById('todo');
let btn = document.getElementById('add');
let pending = document.getElementById('pending-tasks');
let completed = document.getElementById('completed-tasks');
let del = document.querySelectorAll('.delete');
let wrap = document.getElementById('wrapper');
let tasks = document.querySelector('.tasks');

var xhttp = new XMLHttpRequest();

xhttp.open("GET", "http://localhost:3000/db", true);

xhttp.onload = function () {
    let toJson = JSON.parse(this.responseText);
    for(let i=0;i<toJson.pending.length;i++) {
        pending.innerHTML += `<li class="allTasks clearfix">
    <input class="ptasks" type="checkbox" /> 
        <label for="ptasks">${toJson.pending[i].title}</label>
        <button class="delete" data-id=${toJson.pending[i].id}>&#10005;Delete</button></li>`;
    }
    for(let i=0;i<toJson.completed.length;i++){
        completed.innerHTML += `<li class="allTasks clearfix">
    <input class="ctasks" type="checkbox" /> 
        <label for="ctasks">${toJson.completed[i].title}</label>
        <button class="delete" data-id=${toJson.completed[i].id}>&#10005;Delete</button></li>`;
    }
    let check = document.querySelectorAll('input[type=checkbox]');
    for (let i = 0; i < check.length; i++) {
        if (check[i].parentNode.parentNode === completed) {
            check[i].checked = true;
        }
    }
};
xhttp.send();

function sendData() {
    const item = items.value;
    items.value="";
    if (item === "") {
        return alert("Enter something");
    }
    pending.innerHTML += `<li class="allTasks clearfix">
    <input class="ptasks" type="checkbox" /> 
        <label for="ptasks">${item}</label>
        <button class="delete">&#10005;Delete</button></li>`;
    let data = JSON.stringify({
        title: item
    });
    var http = new XMLHttpRequest();
    http.open('POST', 'http://localhost:3000/pending', true);
    http.setRequestHeader('Content-Type', 'application/json');
    http.onload = function () {
        console.log(JSON.parse(this.responseText)['id']);
    };
    http.send(data);
}

wrap.addEventListener('click', function (ev) {
    if (ev.target && ev.target.matches('button.delete')) {
        let datId=ev.target.attributes["data-id"].value;
        var xhr = new XMLHttpRequest();
        if(ev.target.parentNode.parentNode === pending) {
            xhr.open("DELETE", "http://localhost:3000/pending/" + datId, true);
            xhr.onload = function () {
                console.log(this.responseText);
            };
            xhr.send(null);
            return ev.target.parentNode.remove();
        }
        xhr.open("DELETE", "http://localhost:3000/completed/" + datId, true);
        xhr.onload = function () {
            console.log(this.responseText);
        };
        xhr.send(null);
        ev.target.parentNode.remove();
    }
    if (ev.target && ev.target.matches('input[type=checkbox]')) {
        toggleStatus(ev.target);
    }
});

function toggleStatus(checkbox) {
    let clone = checkbox.parentElement.cloneNode(true);
    let data = JSON.stringify({
        title: clone.querySelector('label').innerHTML
    });
    var htt = new XMLHttpRequest();
    if(checkbox.checked){
        completed.appendChild(clone);
        htt.open("POST","http://localhost:3000/completed",true);
    }
    else{
        pending.appendChild(clone);
        htt.open('POST',"http://localhost:3000/pending",true);
    }

    htt.setRequestHeader('Content-Type', 'application/json');
    htt.onload = function () {
            console.log(this.responseText);
        };
    htt.send(data);
    checkbox.parentNode.remove();
    // var httpReq=new XMLHttpRequest();
    //
    // let datId=clone.querySelector('button.delete').attributes['data-id'].value;
    // console.log(clone.parentNode.parentNode);
    // if(clone.parentNode.parentNode === pending) {
    //     httpReq.open("DELETE", "http://localhost:3000/pending/" + datId, true);
    //     httpReq.onload = function () {
    //         console.log(this.responseText);
    //     };
    //     httpReq.send(null);
    //     return checkbox.parentNode.remove();
    // }
    // httpReq.open("DELETE", "http://localhost:3000/completed/" + datId, true);
    // httpReq.onload = function () {
    //     console.log(this.responseText);
    // };
    // httpReq.send(null);
    // return checkbox.parentNode.remove();
}

items.focus();
btn.addEventListener('click', sendData);
items.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        btn.click();
    }
});

tasks.addEventListener('dblclick', function (ev) {
    if (ev.target && ev.target.matches('label')) {
        const t = ev.target.innerHTML;
        ev.target.innerHTML = `<input type="text" value="${t}">`;
        ev.target.addEventListener('keyup', function (event) {
            if (event.key === "Enter") {
                if(ev.target.querySelector('input').value!==null) {
                    ev.target.innerHTML = ev.target.querySelector('input').value;
                }
                if (ev.target.innerHTML === "") {
                    ev.target.parentNode.remove();

                }
            }
        });
    }
});