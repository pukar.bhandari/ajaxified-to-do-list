<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>To-do list</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="wrapper" id="wrapper">
    <h1>To-do list</h1>
    <div class="input clearfix">
        <label for="todo">Add Items</label>
        <input type="text" id="todo" placeholder="What are you doing today?"/>
        <button id="add">Add</button>
    </div>

    <div class="tasks">
        <div class="pending">
            <h3>Pending tasks</h3>
            <ul id="pending-tasks"></ul>
        </div>

        <div class="completed">
            <h3>Completed tasks</h3>
            <ul id="completed-tasks"></ul>
        </div>
    </div>
</div>
<script src="../js/index.js"></script>
</body>
</html>
